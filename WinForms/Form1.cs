﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.DataSource = Unit.DepartmentsRepository.AllItems.Select(x => x.Name).ToList();
            comboBox2.DataSource = Unit.SpecialitiesRepository.AllItems.Select(x => x.Name).ToList();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string faculty = (sender as ComboBox).SelectedItem.ToString();
            listBox1.DataSource = Unit.TeachersRepository.AllItems
                .Where(x => x.Department.Name == faculty)
                //.Select(x => x)
                .ToList();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string spec = (sender as ComboBox).SelectedItem.ToString();
            listBox2.DataSource = Unit.GroupsRepository.AllItems
                //.OrderBy(x => x.Speciality.Name)
                .Where(x => x.Speciality.Name == spec)
                .ToList();
        }
    }
}
